CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `home_id` int(11) DEFAULT NULL,
  `away_id` int(11) DEFAULT NULL,
  `start_time` varchar(200) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2862 DEFAULT CHARSET=utf8;

CREATE TABLE `league` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=456 DEFAULT CHARSET=utf8;

CREATE TABLE `movement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `match_id` int(11) DEFAULT NULL,
  `home_id` int(11) DEFAULT NULL,
  `away_id` int(11) DEFAULT NULL,
  `draw` double DEFAULT NULL,
  `win` double DEFAULT NULL,
  `lost` double DEFAULT NULL,
  `hdp` double DEFAULT NULL,
  `away_hdp` double DEFAULT NULL,
  `home_hdp` double DEFAULT NULL,
  `over_under` double DEFAULT NULL,
  `over` double DEFAULT NULL,
  `under` double DEFAULT NULL,
  `crawl_time` varchar(200) DEFAULT NULL,
  `source` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `home_sign` varchar(45) DEFAULT NULL,
  `away_sign` varchar(45) DEFAULT NULL,
  `win_sign` varchar(45) DEFAULT NULL,
  `draw_sign` varchar(45) DEFAULT NULL,
  `lose_sign` varchar(45) DEFAULT NULL,
  `under_sign` varchar(45) DEFAULT NULL,
  `over_sign` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8346 DEFAULT CHARSET=utf8;

CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) DEFAULT NULL,
  `league_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4727 DEFAULT CHARSET=utf8;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
