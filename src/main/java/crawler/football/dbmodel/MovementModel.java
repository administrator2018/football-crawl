package crawler.football.dbmodel;

import crawler.football.model.Match;

public class MovementModel {
	private int id;
	private int matchId;
	private int homeId;
	private int awayId;
	private Double draw;
	private Double win;
	private Double lost;
	private Double hdp;
	private Double homeHdp;
	private Double awayHdp;
	private Double overUnder;
	private Double over;
	private Double under;
	private String crawlTime;
	private int source;	
	private int status;
	
	private String homeSign;
	private String awaySign;
	private String winSign;
	private String drawSign;
	private String loseSign;
	private String underSign;
	private String overSign;
	
	
	
	
	public MovementModel() {
		// TODO Auto-generated constructor stub
	}
	
	
	public MovementModel(int homeId,int awayId, int status) {
		super();
		this.homeId = homeId;
		this.awayId = awayId;
		this.status = status;
	}
	
	



	public String getHomeSign() {
		return homeSign;
	}


	public void setHomeSign(String homeSign) {
		this.homeSign = homeSign;
	}


	public String getAwaySign() {
		return awaySign;
	}


	public void setAwaySign(String awaySign) {
		this.awaySign = awaySign;
	}


	public String getWinSign() {
		return winSign;
	}


	public void setWinSign(String winSign) {
		this.winSign = winSign;
	}


	public String getDrawSign() {
		return drawSign;
	}


	public void setDrawSign(String drawSign) {
		this.drawSign = drawSign;
	}


	public String getLoseSign() {
		return loseSign;
	}


	public void setLoseSign(String loseSign) {
		this.loseSign = loseSign;
	}


	public String getUnderSign() {
		return underSign;
	}


	public void setUnderSign(String underSign) {
		this.underSign = underSign;
	}


	public String getOverSign() {
		return overSign;
	}


	public void setOverSign(String overSign) {
		this.overSign = overSign;
	}


	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getSource() {
		return source;
	}
	public void setSource(int source) {
		this.source = source;
	}
	public Double getDraw() {
		return draw;
	}
	public void setDraw(Double draw) {
		this.draw = draw;
	}
	public Double getWin() {
		return win;
	}
	public void setWin(Double win) {
		this.win = win;
	}
	public Double getLost() {
		return lost;
	}
	public void setLost(Double lost) {
		this.lost = lost;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMatchId() {
		return matchId;
	}
	public void setMatchId(int matchId) {
		this.matchId = matchId;
	}
	public int getHomeId() {
		return homeId;
	}
	public void setHomeId(int homeId) {
		this.homeId = homeId;
	}
	public int getAwayId() {
		return awayId;
	}
	public void setAwayId(int awayId) {
		this.awayId = awayId;
	}

	public Double getHdp() {
		return hdp;
	}
	public void setHdp(Double hdp) {
		this.hdp = hdp;
	}
	public Double getHomeHdp() {
		return homeHdp;
	}
	public void setHomeHdp(Double homeHdp) {
		this.homeHdp = homeHdp;
	}
	public Double getAwayHdp() {
		return awayHdp;
	}
	public void setAwayHdp(Double awayHdp) {
		this.awayHdp = awayHdp;
	}
	public Double getOverUnder() {
		return overUnder;
	}
	public void setOverUnder(Double overUnder) {
		this.overUnder = overUnder;
	}
	public Double getOver() {
		return over;
	}
	public void setOver(Double over) {
		this.over = over;
	}
	public Double getUnder() {
		return under;
	}
	public void setUnder(Double under) {
		this.under = under;
	}
	public String getCrawlTime() {
		return crawlTime;
	}
	public void setCrawlTime(String crawlTime) {
		this.crawlTime = crawlTime;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + awayId;
		result = prime * result + homeId;
		result = prime * result + status;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovementModel other = (MovementModel) obj;
		if (awayId != other.awayId)
			return false;
		if (homeId != other.homeId)
			return false;
		if (status != other.status)
			return false;
		return true;
	}


	public boolean equalsInDB(Match other) {

		if (awayId != other.getAwayId())
			return false;
		if (homeId != other.getHomeId())
			return false;
		if (status != other.getStatus())
			return false;
		return true;
	}
	
	
}
