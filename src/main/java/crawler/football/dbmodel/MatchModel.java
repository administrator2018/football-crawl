package crawler.football.dbmodel;


public class MatchModel {
	private int id;
	private int homeId;
	private int awayId;
	private String startTime;
	private String url;
	
	
	public MatchModel() {
		// TODO Auto-generated constructor stub
	}
	public MatchModel(int homeId, int awayId, String startTime) {
		super();
		this.homeId = homeId;
		this.awayId = awayId;
		this.startTime = startTime;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getHomeId() {
		return homeId;
	}
	public void setHomeId(int homeId) {
		this.homeId = homeId;
	}
	public int getAwayId() {
		return awayId;
	}
	public void setAwayId(int awayId) {
		this.awayId = awayId;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + awayId;
		result = prime * result + homeId;
		result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatchModel other = (MatchModel) obj;
		if (awayId != other.awayId)
			return false;
		if (homeId != other.homeId)
			return false;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		return true;
	}
	
	
	
}
