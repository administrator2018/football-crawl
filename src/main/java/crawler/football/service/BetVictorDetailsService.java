package crawler.football.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.BrowserType;

import crawler.football.dao.Fixture;
import crawler.football.dao.TeamOLD;
import crawler.football.model.FixOdd;
import crawler.football.model.Handicap;
import crawler.football.model.Match;
import crawler.football.model.OverUnder;

/**
 * Hello world!
 *
 */
public class BetVictorDetailsService extends BaseService {
	private static final Logger logger = LogManager.getLogger(BetVictorDetailsService.class);
	public static String URL = "https://www.betvictor.com/en-gb/sports/coupons/match-betting";

	public void crawl(List<Match> listMatch) throws InterruptedException {
		
		if (listMatch==null || listMatch.size()==0) {
			return ;
		}

		try {
			driver = getWebDriver(BrowserType.CHROME);
			for (Match match : listMatch) {
				try {
					
/*					if (CommonMethod.linkExists(match.getNextURL()) == false) {
						System.out.println("Url not exists: " + match.getNextURL());
						continue;
					}*/
					driver.get(match.getNextURL());
					if (driver.getPageSource().contains("site-error-wrapper")== true) continue;
					FixOdd matchFixOdd = new FixOdd();
					List<WebElement> foiTitles = findElements("#center_content>div.single_markets>div>h4");
					for (WebElement webElement : foiTitles) {
						String title = webElement.getAttribute("title");
						if (title.contains("Match Betting - 90 Mins")) {
							List<WebElement> eHandicaps = findElements(
									webElement.findElement(By.xpath("following-sibling::*")), "tbody>tr>td");
							matchFixOdd.setWin(eHandicaps.get(0).getAttribute("data-sort"));
							matchFixOdd.setDraw(eHandicaps.get(1).getAttribute("data-sort"));
							matchFixOdd.setLost(eHandicaps.get(2).getAttribute("data-sort"));
							match.setFixOdd(matchFixOdd);
						}
					}

					String desc = "";

					List<WebElement> elements = findElements(
							"#center_content > div.coupon_grouping_section > ul > li>a");
					WebElement eHandicap = null;
					for (WebElement webElement2 : elements) {
						if (webElement2.getText().toUpperCase().contains("HANDICAPS")) {
							eHandicap = webElement2;
							break;
						}
					}
					if (eHandicap != null) {
						eHandicap.click();
					}

					List<WebElement> listTitle = findElements("#center_content>div.single_markets>div>h4");
					List<Handicap> handicaps = new ArrayList<Handicap>();
					List<OverUnder> ous = new ArrayList<OverUnder>();
					for (WebElement webElement : listTitle) {
						String title = webElement.getAttribute("title");
						if (title.contains("Asian Handicap") && title.contains("90 Mins")) {
							List<WebElement> eHandicaps = findElements(
									webElement.findElement(By.xpath("following-sibling::*")), "tbody>tr");
							for (WebElement webElement2 : eHandicaps) {
								Handicap handicap = new Handicap();
								WebElement e1 = findElement(webElement2, "td:nth-child(1)");
								WebElement e2 = findElement(webElement2, "td:nth-child(2)");
								desc = findElement(e1, "td:nth-child(1)>span").getAttribute("data-outcome_description");
								System.out.println(desc);
								desc = desc.substring(desc.indexOf("(") + 1, desc.indexOf(")"));
								handicap.setGoal(cleanHdGoal(desc));
								handicap.setHome(e1.getAttribute("data-sort"));
								handicap.setAway(e2.getAttribute("data-sort"));
								handicaps.add(handicap);
							}
						} else if (title.contains("Asian Goal Line") && title.contains("90 Mins")) {
							List<WebElement> eOverUnder = findElements(
									webElement.findElement(By.xpath("following-sibling::*")), "tbody>tr");
							for (WebElement webElement2 : eOverUnder) {
								OverUnder ou = new OverUnder();
								WebElement e1 = findElement(webElement2, "td:nth-child(1)");
								WebElement e2 = findElement(webElement2, "td:nth-child(2)");
								desc = findElement(e1, "td:nth-child(1)>span").getAttribute("data-outcome_description");
								desc = desc.substring(desc.indexOf("(") + 1, desc.indexOf(")"));
								ou.setTotalGoal(cleanHdGoal(desc));
								ou.setOver(e1.getAttribute("data-sort"));
								ou.setUnder(e2.getAttribute("data-sort"));
								ous.add(ou);
							}
						}
					}
					match.setHandicap(handicaps);
					match.setOus(ous);
					//System.out.println(gson.toJson(match));

				} catch (Exception e) {
					logger.error(e);
				}
			}

		} catch (Exception e) {
			logger.error(e);
		} finally {
			driver.close();
		}
	}

	public boolean hasClass(WebElement element, String cssClass) {
		String classes = element.getAttribute("class");
		for (String c : classes.split(" ")) {
			if (c.equals(cssClass)) {
				return true;
			}
		}

		return false;
	}

	public static void main(String[] args) {
		String s = "2018-08-0817:00";
		System.out.println(new BetVictorDetailsService().cleanStartTime(s));
	}

	private String cleanHdGoal(String goal) {
		String result = "0";
		try {
			String[] arrGoal = goal.split(",");
			if (!goal.contains(","))
				result = goal;
			else {
				Double hdc = (Double.parseDouble(arrGoal[1]) + Double.parseDouble(arrGoal[0])) / 2;
				result = hdc.toString();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		if (result != null) {
			result = result.replace("+", "").trim();
		}
		return result;
	}

	private String cleanStartTime(String startTime) {
		SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-ddHH:mm");
		try {
			Date dateValue = input.parse(startTime);
			SimpleDateFormat destDf = new SimpleDateFormat("yyyyMMddHHmm");
			startTime = destDf.format(dateValue);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return startTime;
	}

	private void createTeam(Match match) {
		Integer Home = TeamOLD.checkExist(match.getHome());
		Integer Away = TeamOLD.checkExist(match.getAway());

		if (Home > 0 && Away > 0)
			return;

		if (Home == 0)
			TeamOLD.createTeam(match.getHome(), match.getHome());

		if (Away == 0)
			TeamOLD.createTeam(match.getAway(), match.getAway());
	}

	private void createFixture(Match match, java.util.Date date) {

		Integer Home = TeamOLD.checkExist(match.getHome());
		Integer Away = TeamOLD.checkExist(match.getAway());

		java.sql.Date sqlDate = new java.sql.Date(convertDate(date, match.getStartTime()).getTime());

		Integer FixtureId = Fixture.checkExist(Home, Away, sqlDate);

		if (FixtureId == 0) {
			Fixture.createFixture(Home, Away, convertDate(date, match.getStartTime()));
		}

		FixtureId = Fixture.checkExist(Home, Away, sqlDate);

		java.util.Date utilDate = new java.util.Date();
		java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

		Handicap selectHdc = null;

		for (Handicap hdc : match.getHandicap()) {
			if (Double.parseDouble(hdc.getHome()) >= 1.8 && Double.parseDouble(hdc.getAway()) >= 1.8)
				selectHdc = hdc;
		}

		if (selectHdc == null)
			selectHdc = match.getHandicap().get(0);

		// History.createHistory(FixtureId, currentDate, selectHdc, match.getFixOdd(),
		// match.getOverUnder());

	}

	public Date convertDate(Date myDate, String time) {

		try {
			SimpleDateFormat oldFormat = new SimpleDateFormat("MM-dd-yyyy");

			SimpleDateFormat newFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");

			// Format the date to Strings
			String mdy = oldFormat.format(myDate) + " " + time + ":00";

			return newFormat.parse(mdy);
		} catch (ParseException exp) {
			exp.printStackTrace();
		}
		return null;
	}
}
