package crawler.football.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;

import crawler.football.model.FixOdd;
import crawler.football.model.Handicap;
import crawler.football.model.Match;
import crawler.football.model.OverUnder;
import crawler.football.scheduler.BetvictorMasterTask;

public class M88Service extends BaseService {
	private static final Logger logger = LogManager.getLogger(M88Service.class);

	public List<Match> crawl(String ot, String dt) {
		List<Match> result = new ArrayList<Match>();
		try {
		
			String data = getData(ot, dt);
			//System.out.println(data);
			String[] rows = getRows(data);
			List<Match> matches = new ArrayList<Match>();
			Match match = new Match();
			for (String row : rows) {
				String[] items = getItems(row);
				try {
					match = (Match) match.clone();
				} catch (CloneNotSupportedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				match.setLeague(isBlank(items[2]) == true ? match.getLeague() : items[2].replaceAll("'", "").split("\\|")[0]);
				match.setHome(isBlank(items[6]) == true ? match.getHome() : items[6].replaceAll("'", ""));
				match.setAway(isBlank(items[7]) == true ? match.getAway() : items[7].replaceAll("'", ""));
				match.setStartTime(isBlank(items[5]) == true ? match.getStartTime() : items[5].replaceAll("'", ""));

				FixOdd matchFixOdd = new FixOdd();
				matchFixOdd.setWin(extractItem(items[46]));
				matchFixOdd.setDraw(extractItem(items[48]));
				matchFixOdd.setLost(extractItem(items[47]));
				match.setFixOdd(matchFixOdd);

				OverUnder overUnder = new OverUnder();
				overUnder.setTotalGoal(cleanItem(items[29]));
				overUnder.setOver(extractItem(items[32]));
				overUnder.setUnder(extractItem(items[33]));
				match.setOverUnder(overUnder);

				Handicap hdCap = new Handicap();
				hdCap.setGoal(cleanItem(items[20]));
				hdCap.setHome(extractItem(items[23]));
				hdCap.setAway(extractItem(items[24]));
				match.setHdCap(hdCap);
				matches.add(match);
				//System.out.println(gson.toJson(match));
			}

			
			HashSet<String> homeName = new HashSet<String>();
			for (Match matchItem : matches) {
				Match matchResult = new Match();
				if (homeName.contains(matchItem.getHome()) == false) {
					homeName.add(matchItem.getHome());
					matchResult.setLeague(matchItem.getLeague());
					matchResult.setHome(matchItem.getHome());
					matchResult.setAway(matchItem.getAway());
					matchResult.setStartTime(matchItem.getStartTime());
					List<OverUnder> ous = new ArrayList<OverUnder>();
					List<Handicap> hds = new ArrayList<Handicap>();
					for (Match matchItem2 : matches) {
						if (matchItem2.getHome().equals(matchItem.getHome())) {
							 if (chkFixOdd(matchItem2.getFixOdd())== true) {
								 matchResult.setFixOdd(matchItem2.getFixOdd());
							 }
							 
							 if (chkOverUnder(matchItem2.getOverUnder()) == true) {
								 ous.add(matchItem2.getOverUnder());
							 }
							 
							 if (chkHandicap(matchItem2.getHdCap())== true) {
								 hds.add(matchItem2.getHdCap());
							 }

						}
					}
					matchResult.setOus(ous);
					matchResult.setHandicap(hds);
					result.add(matchResult);
				}
				

			}
/*		
			for (Match matchInfo : result) {
				System.out.println(gson.toJson(matchInfo));
			}*/
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
		return result;

	}

	private boolean chkFixOdd(FixOdd fixOdd) {
		if (StringUtils.isBlank(fixOdd.getWin()) == false && StringUtils.isBlank(fixOdd.getWin()) == false
				&& StringUtils.isBlank(fixOdd.getWin()) == false) {
			return true;
		} else {
			return false;
		}

	}
	
	private boolean chkOverUnder(OverUnder ou) {
		if (StringUtils.isBlank(ou.getTotalGoal()) == false && StringUtils.isBlank(ou.getOver()) == false
				&& StringUtils.isBlank(ou.getUnder()) == false) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean chkHandicap(Handicap hd) {
		if (StringUtils.isBlank(hd.getGoal()) == false && StringUtils.isBlank(hd.getHome()) == false
				&& StringUtils.isBlank(hd.getAway()) == false) {
			return true;
		} else {
			return false;
		}
	}
	

	private String extractItem(String item) {
		String result = item.split("\\|")[0].replaceAll("'", "");
		result = cleanItem(result);
		return result;
	}

	private String cleanItem(String item) {
		String result = StringUtils.substring(item, 0, item.indexOf(".") + 3).replaceAll("'", "");
		return result;
	}

	public static void main(String[] args) {
		M88Service service = new M88Service();
		System.out.println(service.getData("3", "2"));
	}

	private boolean isBlank(String item) {
		return StringUtils.isBlank(item);
	}

	private String getData(String ot, String dt) {
		String data = null;
		try {
			Response response = Jsoup.connect("https://www.m88.com/Main/Sports/mSports/nss/Main2Data.aspx").userAgent(
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36")
					.method(Method.POST).data("spid", "10").data("ot", ot) // 1:Malay, 3: decimal
					// .data("dt", hour)
					.data("dt", dt)// 2:today,21: live,3
					.data("vt", "0").data("vs", "0").data("tf", "0").data("vd", "0").data("lid", "vi")
					.data("lgnum", "999").data("reqpart", "0").data("verpart", "").data("prevParams", "")
					.data("verpartLA", "").followRedirects(true).execute();
			data = response.parse().toString();
			//System.out.println(data);
		} catch (IOException e) {
			logger.error("getData M88 error:"+ e);
		}
		return data;
	}

	private String[] getRows(String data) {
		data = StringUtils.substringBetween(data, "d=[[", "],['");
		String[] rows = data.split("],");
		return rows;
	}

	private String[] getItems(String row) {
		row = row.replace("[", "").replace("\n", "");
		String[] items = row.split(",");
		return items;
	}

}
