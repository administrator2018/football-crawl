package crawler.football.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.BrowserType;

import crawler.football.dao.Fixture;
import crawler.football.dao.TeamOLD;
import crawler.football.model.FixOdd;
import crawler.football.model.Handicap;
import crawler.football.model.Match;
import crawler.football.utils.DateTimeUtil;

/**
 * Hello world!
 *
 */
public class BetVictorMasterService extends BaseService {
	private static final Logger logger = LogManager.getLogger(BetVictorMasterService.class);
	public static String URL = "https://www.betvictor.com/en-gb/sports/coupons/match-betting";

	public List<Match> crawl() {
		List<Match> listMatch = new ArrayList<Match>();
		try {
			driver = getWebDriver(BrowserType.CHROME);
			driver.get(URL);
			WebElement webE = findElement("#full_width_head_content > div > div");
			Actions action = new Actions(driver);
			action.moveToElement(webE).perform();
			findElement("li.odds-selector-component__item:nth-child(3) > a:nth-child(1)").click();

			List<WebElement> rows = findElements("#three_way_outright_coupon-markets > table[role=grid]>tbody>tr");
			for (WebElement row : rows) {
				try {
					Match match = new Match();
					String startTime = "";
					String day = findElement(row, "td.date").getAttribute("data-sort").substring(0, 10);
					String time = findElement(row, "td.date>span").getText();
					if (time != null && time.contains("LIVE")) {
						time = DateTimeUtil.getCurrTime();
					}
					startTime = cleanStartTime(day + time);
					match.setStartTime(startTime);
					String desc = findElement(row, "td.event_description").getText();
					String[] matchTeam = desc.split(" v ");
					match.setHome(matchTeam[0]);
					match.setAway(matchTeam[1]);
					match.setLeague(findElement(row, "td.event_description > span").getText());
					FixOdd matchFixOdd = new FixOdd();
					match.setNextURL(findElement(row, "td.event_description>a").getAttribute("href"));
					match.setFixOdd(matchFixOdd);
					listMatch.add(match);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			System.out.println(gson.toJson(listMatch));

		} catch (Exception e) {
			logger.error(e);
		} finally {
			if (driver != null) {
				driver.close();
			}

		}
		return listMatch;
		// }
	}

	public static void main(String[] args) {
		String s = "Mon 06 Aug 2018 17:20";
		System.out.println(new BetVictorMasterService().cleanStartTime(s));
	}

	private String cleanHdGoal(String goal) {
		String result = "0";
		try {
			String[] arrGoal = goal.split(",");
			if (!goal.contains(","))
				result = goal;
			else {
				Double hdc = (Double.parseDouble(arrGoal[1]) + Double.parseDouble(arrGoal[0])) / 2;
				result = hdc.toString();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		if (result != null) {
			result = result.replace("+", "").trim();
		}
		return result;
	}

	private String cleanStartTime(String startTime) {
		SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-ddHH:mm");
		try {
			Date dateValue = input.parse(startTime);
			SimpleDateFormat destDf = new SimpleDateFormat("yyyyMMddHHmm");
			startTime = destDf.format(dateValue);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return startTime;
	}

	private void createTeam(Match match) {
		Integer Home = TeamOLD.checkExist(match.getHome());
		Integer Away = TeamOLD.checkExist(match.getAway());

		if (Home > 0 && Away > 0)
			return;

		if (Home == 0)
			TeamOLD.createTeam(match.getHome(), match.getHome());

		if (Away == 0)
			TeamOLD.createTeam(match.getAway(), match.getAway());
	}

	private void createFixture(Match match, java.util.Date date) {

		Integer Home = TeamOLD.checkExist(match.getHome());
		Integer Away = TeamOLD.checkExist(match.getAway());

		java.sql.Date sqlDate = new java.sql.Date(convertDate(date, match.getStartTime()).getTime());

		Integer FixtureId = Fixture.checkExist(Home, Away, sqlDate);

		if (FixtureId == 0) {
			Fixture.createFixture(Home, Away, convertDate(date, match.getStartTime()));
		}

		FixtureId = Fixture.checkExist(Home, Away, sqlDate);

		java.util.Date utilDate = new java.util.Date();
		java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

		Handicap selectHdc = null;

		for (Handicap hdc : match.getHandicap()) {
			if (Double.parseDouble(hdc.getHome()) >= 1.8 && Double.parseDouble(hdc.getAway()) >= 1.8)
				selectHdc = hdc;
		}

		if (selectHdc == null)
			selectHdc = match.getHandicap().get(0);

		//History.createHistory(FixtureId, currentDate, selectHdc, match.getFixOdd(), match.getOverUnder());

	}

	public Date convertDate(Date myDate, String time) {

		try {
			SimpleDateFormat oldFormat = new SimpleDateFormat("MM-dd-yyyy");

			SimpleDateFormat newFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");

			// Format the date to Strings
			String mdy = oldFormat.format(myDate) + " " + time + ":00";

			return newFormat.parse(mdy);
		} catch (ParseException exp) {
			exp.printStackTrace();
		}
		return null;
	}
}
