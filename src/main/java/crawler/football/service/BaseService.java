package crawler.football.service;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import crawler.football.common.WebDriverFactory;

public class BaseService {
	protected  Gson  gson;
	protected WebDriver driver;
	public BaseService() {
		gson = new GsonBuilder().disableHtmlEscaping().create();
	}
	public WebDriver getWebDriver(String browserType) {
		DesiredCapabilities capabilities = new DesiredCapabilities();

		if (browserType.equals(BrowserType.PHANTOMJS)) {
			capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "E:/phantomjs.exe");

			capabilities.setJavascriptEnabled(true);
			capabilities.setCapability("takesScreenshot", false);
			capabilities.setBrowserName(BrowserType.PHANTOMJS);
			capabilities.setPlatform(Platform.ANY);

			capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
					new String[] { "--web-security=false", "--ssl-protocol=any", "--ignore-ssl-errors=true",
							"--load-images=yes", "--disk-cache=true", "--output-encoding=utf-8" });
		} else if (browserType.equals(BrowserType.CHROME)) {
			System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			// options.addArguments("-incognito");
			options.addArguments("--disable-popup-blocking");

			capabilities.setJavascriptEnabled(true);
			capabilities.setCapability("takesScreenshot", false);
			capabilities.setBrowserName(BrowserType.CHROME);
			capabilities.setPlatform(Platform.ANY);
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		}

		driver = WebDriverFactory.getDriver(capabilities);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		return driver;
	}
	public WebElement findElement(String css) {
		return driver.findElement(By.cssSelector(css));
	}
	public WebElement findElement(WebElement webElement, String css) {
		return webElement.findElement(By.cssSelector(css));
	}	
	public List<WebElement> findElements(String css) {
		return driver.findElements(By.cssSelector(css));
	}
	
	public List<WebElement> findElements(WebElement webElement,String css) {
		return webElement.findElements(By.cssSelector(css));
	}	
}
