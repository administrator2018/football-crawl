package crawler.football.scheduler;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import crawler.football.App;
import crawler.football.dao.LeagueDao;
import crawler.football.dao.MatchDao;
import crawler.football.dao.MovementDao;
import crawler.football.dao.TeamDao;
import crawler.football.dbmodel.MatchModel;
import crawler.football.dbmodel.MovementModel;
import crawler.football.model.Match;
import crawler.football.service.M88Service;
import java.util.Optional;

public class SignTask extends BaseTask {
	private static final Logger logger = LogManager.getLogger(SignTask.class);
	M88Service service;
	LeagueDao leagueDao;
	TeamDao teamDao;
	MatchDao matchDao;

	public SignTask() {
		service = new M88Service();
		leagueDao = new LeagueDao();
		teamDao = new TeamDao();
		matchDao = new MatchDao();

	}

	@Override
	public void run() {
		logger.info("Start SignTask master");
		List<MovementModel> movs = movementDao.findAll();
		movs.forEach(mov -> {
			process(mov, movs);
		});
		logger.info("End SignTask master");
	}

	public void process(MovementModel mov, List<MovementModel> movs) {
		List<MovementModel> listMov = movs.stream().filter(item -> item.getHomeId() == mov.getHomeId()
				&& item.getAwayId() == mov.getAwayId() && item.getMatchId() == mov.getMatchId())
				.collect(Collectors.toList());

		listMov.sort(Comparator.comparing(MovementModel::getStatus));
		/*
		 * if (listMov.size()>8) { listMov.forEach(x ->
		 * System.out.println(x.getMatchId())); } else {
		 * 
		 * }
		 */
		listMov.forEach(currMov -> {
			int status = currMov.getStatus();
			Optional<MovementModel> preMovOpt;
			switch (status) {
			case 2:
			case 3:
				preMovOpt = listMov.stream().filter(x -> x.getStatus() == status - 1).findFirst();
				preMovOpt.ifPresent(preMov -> {
					if (currMov.getHomeHdp() >= preMov.getHomeHdp()) {
						currMov.setHomeSign("V");
					} else {
						currMov.setHomeSign("X");
					}

					if (currMov.getAwayHdp() >= preMov.getAwayHdp()) {
						currMov.setAwaySign("V");
					} else {
						currMov.setAwaySign("X");
					}

					if (currMov.getWin() >= preMov.getWin()) {
						preMov.setWinSign("V");
					} else {
						currMov.setWinSign("X");
					}

					if (currMov.getDraw() >= preMov.getDraw()) {
						currMov.setDrawSign("V");
					} else {
						currMov.setDrawSign("X");
					}

					if (currMov.getLost() >= preMov.getLost()) {
						currMov.setLoseSign("V");
					} else {
						currMov.setLoseSign("X");
					}

					if (currMov.getOverUnder() == preMov.getOverUnder()) {
						if (currMov.getUnder() >= preMov.getUnder()) {
							currMov.setUnderSign("V");
						} else {
							currMov.setUnderSign("X");
						}

						if (currMov.getOver() >= preMov.getOver()) {
							currMov.setOverSign("V");
						} else {
							currMov.setOverSign("X");
						}
					} else {
						if (currMov.getOverUnder() > preMov.getOverUnder()) {
							currMov.setOverSign("V");
						} else {
							currMov.setOverSign("X");
						}
					}

					movementDao.update(currMov);
					movementDao.update(preMov);

				});

				break;
			case 4:
				preMovOpt = listMov.stream().filter(x -> x.getStatus() == status - 1).findFirst();
				preMovOpt.ifPresent(preMov -> {
					if (currMov.getHomeHdp() >= preMov.getHomeHdp()) {
						preMov.setHomeSign("V");
						currMov.setHomeSign("V");
					} else {
						preMov.setHomeSign("X");
						currMov.setHomeSign("X");
					}

					if (currMov.getAwayHdp() >= preMov.getAwayHdp()) {
						preMov.setAwaySign("V");
						currMov.setAwaySign("V");
					} else {
						preMov.setAwaySign("X");
						currMov.setAwaySign("X");
					}

					if (currMov.getWin() >= preMov.getWin()) {
						preMov.setWinSign("V");
						currMov.setWinSign("V");
					} else {
						preMov.setWinSign("X");
						currMov.setWinSign("X");
					}

					if (currMov.getDraw() >= preMov.getDraw()) {
						preMov.setDrawSign("V");
						currMov.setDrawSign("V");
					} else {
						preMov.setDrawSign("X");
						currMov.setDrawSign("X");
					}

					if (currMov.getLost() >= preMov.getLost()) {
						preMov.setLoseSign("V");
						currMov.setLoseSign("V");
					} else {
						preMov.setLoseSign("X");
						currMov.setLoseSign("X");
					}

					if (currMov.getOverUnder() == preMov.getOverUnder()) {
						if (currMov.getUnder() >= preMov.getUnder()) {
							preMov.setUnderSign("V");
							currMov.setUnderSign("V");
						} else {
							preMov.setUnderSign("X");
							currMov.setUnderSign("X");
						}

						if (currMov.getOver() >= preMov.getOver()) {
							preMov.setOverSign("V");
							currMov.setOverSign("V");
						} else {
							preMov.setOverSign("X");
							currMov.setOverSign("V");
						}
					} else {
						if (currMov.getOverUnder() > preMov.getOverUnder()) {
							preMov.setUnderSign("V");
							preMov.setOverSign("V");
							currMov.setUnderSign("V");
							currMov.setOverSign("V");
						} else {
							preMov.setUnderSign("X");
							preMov.setOverSign("X");
							currMov.setUnderSign("X");
							currMov.setOverSign("X");
						}
					}

					movementDao.update(currMov);
					movementDao.update(preMov);

				});
				break;

			default:
				break;
			}
		});

	}

}
