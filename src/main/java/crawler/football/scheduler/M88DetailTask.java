package crawler.football.scheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import crawler.football.dao.LeagueDao;
import crawler.football.dao.MatchDao;
import crawler.football.dao.MovementDao;
import crawler.football.dao.TeamDao;
import crawler.football.dbmodel.MatchModel;
import crawler.football.dbmodel.MovementModel;
import crawler.football.model.Match;
import crawler.football.service.M88Service;
import crawler.football.utils.CommonMethod;
import crawler.football.utils.DateTimeUtil;

public class M88DetailTask extends BaseTask {
	private static final Logger logger = LogManager.getLogger(M88DetailTask.class);
	M88Service service;
	LeagueDao leagueDao;
	TeamDao teamDao;
	MatchDao matchDao;
	MovementDao movementDao;

	public M88DetailTask() {
		service = new M88Service();
		leagueDao = new LeagueDao();
		teamDao = new TeamDao();
		matchDao = new MatchDao();
		movementDao = new MovementDao();
	}

	@Override
	public void run() {
		// 0. 30p, 1h-60, 2h-120, 4h-240 trước trận
		// 0. Task run 2 seconds
		// 1. get list match today
		// 2. Loop list match
		// 3. If (match.startTime - now betwin 4 and 2 , not yet crawl)=> add to current
		// crawl list
		// 4. Crawl current crawl list
		logger.info("Start M88DetailTask detail");
		List<MatchModel> matchModels = matchDao.findAll();
		List<Match> listMatch = new ArrayList<Match>();
		List<Match> listMatchCrawl = service.crawl("3", "2");
		logger.info("listMatchCrawl:"+ new Gson().toJson(listMatchCrawl));
		//saveMaster(listMatchCrawl);
		HashMap<Integer, Match> hmMatches = new HashMap<>();
		for (Match match : listMatchCrawl) {
			int homeId = teamDao.getIdByName(match.getHome());
			hmMatches.put(homeId, match);
		}

		//List<Match> currMatches = new ArrayList<>();
		for (MatchModel matchInfo : matchModels) {
			String matchTime = matchInfo.getStartTime();
			String currTime = DateTimeUtil.getCurrDateTime();
			int minutes = DateTimeUtil.getTimeDiffe(currTime, matchTime, "yyyyMMddHHmm", "mm");
			int status = CommonMethod.getStatus(minutes);
			MovementModel mov = movementDao.findByMatch(matchInfo);
			if (minutes - 1 < 0) {
				continue;
			}
			if ((mov == null && status == 1) || (mov != null && mov.getStatus() + 1 == status)) {
				try {

					Match match = new Match();
					match = hmMatches.get(matchInfo.getHomeId());
					int homeId = matchInfo.getHomeId();
					int awayId = matchInfo.getAwayId();
					logger.info("matchInfo.getHomeId():"+matchInfo.getHomeId());
					match.setStartTime(matchInfo.getStartTime());
					match.setHome(teamDao.getNameById(homeId));
					match.setAway(teamDao.getNameById(awayId));
					match.setNextURL(matchInfo.getUrl());
					match.setStatus(status);
					match.setHomeId(homeId);
					match.setAwayId(awayId);
					listMatch.add(match);
				} catch (Exception e) {
					logger.error(e);
				}
			}
		}
		for (Match match : listMatch) {
			System.out.println(new Gson().toJson(match));
		}
		// 5. remove existed in DB
		// listMatch.removeIf(x -> matchModels.contains(x));
		//remove existed movement
		List<MovementModel> movs = movementDao.findAll();
		listMatch.removeIf(x -> movs.contains(new MovementModel(x.getHomeId(), x.getAwayId(), x.getStatus())));
		saveMov(listMatch,2);
		logger.info("End M88DetailTask detail");
		//saveMov(currMatches, 2);

	}

}
