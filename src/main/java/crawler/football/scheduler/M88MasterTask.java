package crawler.football.scheduler;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import crawler.football.App;
import crawler.football.dao.LeagueDao;
import crawler.football.dao.MatchDao;
import crawler.football.dao.TeamDao;
import crawler.football.model.Match;
import crawler.football.service.M88Service;

public class M88MasterTask extends BaseTask {
	private static final Logger logger = LogManager.getLogger(M88MasterTask.class);
	M88Service service;
	LeagueDao leagueDao;
	TeamDao teamDao;
	MatchDao matchDao;

	public M88MasterTask() {
		service = new M88Service();
		leagueDao = new LeagueDao();
		teamDao = new TeamDao();
		matchDao = new MatchDao();
	}

	@Override
	public void run() {
		logger.info("Start M88MasterTask master");
		List<Match> matches = service.crawl("3", "2");
		saveMaster(matches);
		logger.info("End M88MasterTask master");
	}

}
