package crawler.football.scheduler;

import java.util.List;
import java.util.TimerTask;

import crawler.football.dao.LeagueDao;
import crawler.football.dao.MatchDao;
import crawler.football.dao.MovementDao;
import crawler.football.dao.TeamDao;
import crawler.football.dbmodel.LeagueModel;
import crawler.football.dbmodel.MatchModel;
import crawler.football.dbmodel.MovementModel;
import crawler.football.dbmodel.TeamModel;
import crawler.football.model.FixOdd;
import crawler.football.model.Handicap;
import crawler.football.model.Match;
import crawler.football.model.OverUnder;

public class BaseTask extends TimerTask {
	LeagueDao leagueDao;
	TeamDao teamDao;
	MatchDao matchDao;
	MovementDao movementDao;

	public BaseTask() {
		leagueDao = new LeagueDao();
		teamDao = new TeamDao();
		matchDao = new MatchDao();
		movementDao = new MovementDao();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}

	public void saveMaster(List<Match> matches) {
		List<MatchModel> matchesInDb = matchDao.findAll();
		matches.removeIf(x -> matchesInDb.contains(new MatchModel( teamDao.getIdByName(x.getHome()), teamDao.getIdByName(x.getAway()),x.getStartTime())));
		for (Match match : matches) {
			LeagueModel leagueModel = new LeagueModel();
			leagueModel.setName(match.getLeague());
			leagueDao.save(leagueModel);
			int leagueId = leagueDao.getIdByName(leagueModel.getName());

			TeamModel homeTeam = new TeamModel();
			homeTeam.setLeagueId(leagueId);
			homeTeam.setName(match.getHome());
			teamDao.save(homeTeam);

			TeamModel awayTeam = new TeamModel();
			awayTeam.setLeagueId(leagueId);
			awayTeam.setName(match.getAway());
			teamDao.save(awayTeam);

			MatchModel matchModel = new MatchModel();
			int homeId = teamDao.getIdByName(match.getHome());
			matchModel.setHomeId(homeId);
			int awayId = teamDao.getIdByName(match.getAway());
			matchModel.setAwayId(awayId);
			matchModel.setStartTime(match.getStartTime());
			matchModel.setUrl(match.getNextURL());
			matchDao.save(matchModel);

		}
	}

	public void saveMov(List<Match> currMatches,int source ) {
		
		for (Match match : currMatches) {
			try {
				MatchModel matchModel = new MatchModel();
				int homeId = teamDao.getIdByName(match.getHome());
				matchModel.setHomeId(homeId);
				int awayId = teamDao.getIdByName(match.getAway());
				matchModel.setAwayId(awayId);
				matchModel.setStartTime(match.getStartTime());
				int matchId = matchDao.getId(matchModel);
				MovementModel movementModel = new MovementModel();
				movementModel.setCrawlTime(match.getStartTime());
				movementModel.setMatchId(matchId);
				movementModel.setHomeId(match.getHomeId());
				movementModel.setAwayId(match.getAwayId());
				FixOdd fixOdd = match.getFixOdd();
				if (fixOdd != null) {
					movementModel.setDraw(Double.parseDouble(fixOdd.getDraw()));
					movementModel.setWin(Double.parseDouble(fixOdd.getWin()));
					movementModel.setLost(Double.parseDouble(fixOdd.getLost()));
				}
				List<Handicap> hdps = match.getHandicap();
				Double baseValue = 1.8D;
				if (hdps != null && hdps.size() != 0) {

					// DK1: home value va away value>1.8
					boolean chk1 = false;
					for (Handicap hdp : hdps) {
						Double homeValue = Double.parseDouble(hdp.getHome());
						Double awayValue = Double.parseDouble(hdp.getAway());
						if (homeValue >= baseValue && awayValue >= baseValue) {
							movementModel.setHdp(Double.parseDouble(hdp.getGoal()));
							movementModel.setHomeHdp(Double.parseDouble(hdp.getHome()));
							movementModel.setAwayHdp(Double.parseDouble(hdp.getAway()));
							chk1 = true;
							break;
						}
					}
					if (chk1 == false) {
						Double minOffset = 0D;
						for (Handicap hdp : hdps) {
							Double homeValue = Double.parseDouble(hdp.getHome());
							Double awayValue = Double.parseDouble(hdp.getAway());
							Double currOffset = Math.abs(homeValue - awayValue);
							if (currOffset < minOffset || minOffset == 0) {
								minOffset = currOffset;
								movementModel.setHdp(Double.parseDouble(hdp.getGoal()));
								movementModel.setHomeHdp(Double.parseDouble(hdp.getHome()));
								movementModel.setAwayHdp(Double.parseDouble(hdp.getAway()));
							}
						}

					}

				}
				List<OverUnder> ous = match.getOus();
				if (ous != null) {
					// DK1: over value va under value>1.8
					boolean chk1 = false;
					for (OverUnder ou : ous) {
						Double overValue = Double.parseDouble(ou.getOver());
						Double underValue = Double.parseDouble(ou.getUnder());
						if (overValue >= baseValue && underValue >= baseValue) {
							movementModel.setOverUnder(Double.parseDouble(ou.getTotalGoal()));
							movementModel.setOver(Double.parseDouble(ou.getOver()));
							movementModel.setUnder(Double.parseDouble(ou.getUnder()));
							chk1 = true;
							break;
						}
					}

					if (chk1 == false) {
						Double minOffset = 0D;
						for (OverUnder ou : ous) {
							Double overValue = Double.parseDouble(ou.getOver());
							Double underValue = Double.parseDouble(ou.getUnder());
							Double currOffset = Math.abs(overValue - underValue);
							if (currOffset < minOffset || minOffset == 0) {
								minOffset = currOffset;
								movementModel.setOverUnder(Double.parseDouble(ou.getTotalGoal()));
								movementModel.setOver(Double.parseDouble(ou.getOver()));
								movementModel.setUnder(Double.parseDouble(ou.getUnder()));
							}
						}

					}

				}
				movementModel.setSource(source);
				movementModel.setStatus(match.getStatus());
				movementDao.save(movementModel);
			} catch (Exception e) {
				System.out.println("Loi url:" + match.getNextURL());
				e.printStackTrace();
			}
		}
	}

}
