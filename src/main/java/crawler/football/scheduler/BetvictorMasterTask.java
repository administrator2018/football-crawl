package crawler.football.scheduler;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import crawler.football.dao.LeagueDao;
import crawler.football.dao.MatchDao;
import crawler.football.dao.TeamDao;
import crawler.football.model.Match;
import crawler.football.service.BetVictorMasterService;

public class BetvictorMasterTask extends BaseTask {
	private static final Logger logger = LogManager.getLogger(BetvictorMasterTask.class);
	BetVictorMasterService service;
	LeagueDao leagueDao;
	TeamDao teamDao;
	MatchDao matchDao;

	public BetvictorMasterTask() {
		service = new BetVictorMasterService();
		leagueDao = new LeagueDao();
		teamDao = new TeamDao();
		matchDao = new MatchDao();
	}

	@Override
	public void run() {
		logger.info("Start BetvictorMasterTask master");
		List<Match> matches = service.crawl();
		saveMaster(matches);
		logger.info("End BetvictorMasterTask master");
	}

}
