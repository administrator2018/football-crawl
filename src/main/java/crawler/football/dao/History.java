package crawler.football.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import crawler.football.model.FixOdd;
import crawler.football.model.Handicap;
import crawler.football.model.OverUnder;

public class History {

	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DATABASE_URL = "jdbc:mysql://103.252.253.88:3307/football";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "1132426aA@";

	public static void createHistory(Integer FixtureId, java.util.Date Time, Handicap hdc, FixOdd fix, OverUnder ou) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			preparedStatement = connection.prepareStatement("INSERT INTO History(FixtureId, Time, Hd, Hd1, Hd2, H, D, L, T, U, O) VALUES(?,?,?,?,?,?,?,?,?,?,?)");
			preparedStatement.setInt(1,  FixtureId);
			preparedStatement.setObject(2, new java.sql.Timestamp(Time.getTime()));
			preparedStatement.setDouble(3, hdc.getGoal() == "W" ? 0d : Double.parseDouble(hdc.getGoal()));
			preparedStatement.setDouble(4, Double.parseDouble(hdc.getHome()));
			preparedStatement.setDouble(5, Double.parseDouble(hdc.getAway()));
/*			preparedStatement.setDouble(6, fix.getWin());
			preparedStatement.setDouble(7, fix.getDraw());
			preparedStatement.setDouble(8, fix.getLost());*/
			
			try{
				preparedStatement.setDouble(9, Double.parseDouble(ou.getTotalGoal()));
			}catch (Exception ex){
				preparedStatement.setDouble(9, 0d);
			}
			
			try{
				preparedStatement.setDouble(10, Double.parseDouble(ou.getUnder()));
			}catch (Exception ex){
				preparedStatement.setDouble(10, 0d);
			}
			
			try{
				preparedStatement.setDouble(11, Double.parseDouble(ou.getOver()));
			}catch (Exception ex){
				preparedStatement.setDouble(11, 0d);
			}
			
			boolean b = preparedStatement.execute();
			if (b == true)
				System.out.println("1 record inserted...");
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}
	
	public static Integer checkExist(Integer FixtureId, Date Time) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			preparedStatement = connection.prepareStatement("Select HistoryId from History where FixtureId = ? and Time = ?");
			preparedStatement.setInt(1, FixtureId);
			preparedStatement.setDate(2, Time);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				return (Integer) rs.getObject(1);
			}
			return 0;
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return 0;
	}

	public static void deleteHistory() {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			preparedStatement = connection.prepareStatement("DELETE FROM HISTORY WHERE TIMESTAMPDIFF(DAY, HISTORY.time, now()) > 3");
			boolean b = preparedStatement.execute();
			if (b == true)
				System.out.println("1 record deleted...");
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}

	public static int checkExist(String value) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			preparedStatement = connection.prepareStatement("Select FixtureId from Fixture where value = ?");
			preparedStatement.setString(1, value);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				return (Integer) rs.getObject(1);
			}
			return 0;
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return 0;
	}
	
	
}
