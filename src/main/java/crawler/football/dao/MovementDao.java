package crawler.football.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import crawler.football.dbmodel.MatchModel;
import crawler.football.dbmodel.MovementModel;
import crawler.football.utils.DateTimeUtil;

public class MovementDao {

	public void save(MovementModel movement) {
		String sql = "INSERT INTO movement(match_id, home_id, away_id, draw, win, lost, hdp, home_hdp, away_hdp, over_under, `over`, under, crawl_time, source, status) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?, ?, ?)";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {
			preStm.setInt(1, movement.getMatchId());
			preStm.setInt(2, movement.getHomeId());
			preStm.setInt(3, movement.getAwayId());
			preStm.setDouble(4, movement.getDraw() == null ? 0d : movement.getDraw());
			preStm.setDouble(5, movement.getWin() == null ? 0d : movement.getWin());
			preStm.setDouble(6, movement.getLost() == null ? 0d : movement.getLost());
			preStm.setDouble(7, movement.getHdp() == null ? 0d : movement.getHdp());
			preStm.setDouble(8, movement.getHomeHdp() == null ? 0d : movement.getHomeHdp());
			preStm.setDouble(9, movement.getAwayHdp() == null ? 0d : movement.getAwayHdp());
			preStm.setDouble(10, movement.getOverUnder() == null ? 0d : movement.getOverUnder());
			preStm.setDouble(11, movement.getOver() == null ? 0d : movement.getOver());
			preStm.setDouble(12, movement.getUnder() == null ? 0d : movement.getUnder());
			preStm.setString(13, DateTimeUtil.getCurrDateTime());
			preStm.setInt(14, movement.getSource());
			preStm.setInt(15, movement.getStatus());
			preStm.execute();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}

	public void update(MovementModel movement) {
		String sql = "update movement set home_sign=?, away_sign =?, win_sign=?, draw_sign=?, lose_sign=?, under_sign=?, over_sign=?";

		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {
			preStm.setString(1, movement.getHomeSign());
			preStm.setString(2, movement.getAwaySign());
			preStm.setString(3, movement.getWinSign());
			preStm.setString(4, movement.getDrawSign());
			preStm.setString(5, movement.getLoseSign());
			preStm.setString(6, movement.getUnderSign());
			preStm.setString(7, movement.getOverSign());
			preStm.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}

	public List<MovementModel> findAll() {
		List<MovementModel> matchesInDb = new ArrayList<>();
		String sql = "select * from movement";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {

			ResultSet rs = preStm.executeQuery();

			while (rs.next()) {
				MovementModel model = new MovementModel();
				model.setId(rs.getInt("id"));
				model.setMatchId(rs.getInt("match_id"));
				model.setHomeId(rs.getInt("home_id"));
				model.setAwayId(rs.getInt("away_id"));
				model.setDraw(rs.getDouble("draw"));
				model.setWin(rs.getDouble("win"));
				model.setLost(rs.getDouble("lost"));
				model.setHdp(rs.getDouble("hdp"));
				model.setAwayHdp(rs.getDouble("away_hdp"));
				model.setHomeHdp(rs.getDouble("home_hdp"));
				model.setOverUnder(rs.getDouble("over_under"));
				model.setOver(rs.getDouble("over"));
				model.setUnder(rs.getDouble("under"));
				model.setCrawlTime(rs.getString("crawl_time"));
				model.setSource(rs.getInt("source"));
				model.setStatus(rs.getInt("status"));
				matchesInDb.add(model);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return matchesInDb;
	}

	public void save(List<MovementModel> leagues) {
		for (MovementModel movement : leagues) {
			save(movement);
		}
	}

	public MovementModel findByMatch(MatchModel match) {

		MovementModel mov = null;
		String sql = "select home_id,away_id , max(status) status from movement where home_id = ? and away_id=? group by home_id,away_id";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {
			preStm.setInt(1, match.getHomeId());
			preStm.setInt(2, match.getAwayId());
			ResultSet rs = preStm.executeQuery();
			if (rs.next()) {
				mov = new MovementModel();
				mov.setHomeId(rs.getInt("home_id"));
				mov.setAwayId(rs.getInt("away_id"));
				mov.setStatus(rs.getInt("status"));
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return mov;
	}

}
