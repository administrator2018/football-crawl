package crawler.football.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

public class Fixture {

	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DATABASE_URL = "jdbc:mysql://103.252.253.88:3307/football";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "1132426aA@";//DB local khong co mat khau

	public static void createFixture(Integer TeamId1, Integer TeamId2, java.util.Date date) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			preparedStatement = connection.prepareStatement("INSERT INTO Fixture(Id1,Id2, TIME) VALUES(?,?,?)");
			preparedStatement.setLong(1, TeamId1);
			preparedStatement.setLong(2, TeamId2);
			preparedStatement.setObject(3, new java.sql.Timestamp(date.getTime()));
			boolean b = preparedStatement.execute();
			if (b == true)
				System.out.println("1 record inserted...");
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}
	
	public static Integer checkExist(Integer HomeId, Integer AwayId, Date date) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			preparedStatement = connection.prepareStatement("Select FixtureId from Fixture where Id1 = ? and Id2 = ? and Date(TIME) = Date(?)");
			preparedStatement.setLong(1, HomeId);
			preparedStatement.setLong(2, AwayId);
			preparedStatement.setDate(3, date);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				return (Integer) rs.getObject(1);
			}
			return 0;
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return 0;
	}
	
	public static boolean checkExistToCrawl() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT FixtureId FROM fixture                                         ");
		sb.append("where ( TIMESTAMPDIFF(MINUTE, now(), FIXTURE.time) between 1 and 9    ");
		sb.append("or TIMESTAMPDIFF(MINUTE, now(), FIXTURE.time) between 20 and 29       ");
		sb.append("or TIMESTAMPDIFF(MINUTE, now(), FIXTURE.time) between 40 and 49       ");
		sb.append("or TIMESTAMPDIFF(MINUTE, now(), FIXTURE.time) between 220 and 239     ");
		sb.append(")                                                                     ");
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			preparedStatement = connection.prepareStatement(sb.toString());
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				return true;
			}
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return false;
	}
	
	public static boolean checkIfRuntoday() {
		boolean blnReturn = false;
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT FixtureId FROM fixture 	");
		sb.append("where hour(TIME) = hour(now())   ");
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			preparedStatement = connection.prepareStatement(sb.toString());
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				blnReturn = true;
				break;
			}
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return blnReturn;
	}

	public static boolean readFixtures() {
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			statement = connection.createStatement();
			
			StringBuilder sb = new StringBuilder();
			
			sb.append("SELECT                                                ");
			sb.append("  fixture.Id1,                                    ");
			sb.append("  fixture.Id2,                                    ");
			sb.append("  fixture.Date                                        ");
			sb.append("  ,tmpTmp.COUNT                                       ");
			sb.append("FROM                                                  ");
			sb.append("  fixture                                             ");
			sb.append("LEFT JOIN                                             ");
			sb.append("(                                                     ");
			sb.append("  SELECT                                              ");
			sb.append("    fixture.FixtureId                                 ");
			sb.append("    ,COUNT(1) count                                   ");
			sb.append("  FROM                                                ");
			sb.append("    fixture                                           ");
			sb.append("   INNER JOIN                                         ");
			sb.append("    history                                           ");
			sb.append("    ON                                                ");
			sb.append("    fixture.FixtureId = history.FixtureId             ");
			sb.append("  GROUP BY                                            ");
			sb.append("    fixture.FixtureId                                 ");
			sb.append(") tmpTmp                                              ");
			sb.append("ON                                                    ");
			sb.append("fixture.FixtureId = tmpTmp.FixtureId                  ");
			sb.append("WHERE tmpTmp.COUNT IS NOT NULL                        ");
			sb.append("and DATE(fixture.date) = DATE(CURDATE())              ");
			
			sb.append("  ORDER BY                                            ");
			sb.append("  fixture.Date desc                                   ");
			
			ResultSet resultSet = statement.executeQuery(sb.toString());
			
			ResultSetMetaData metaData = resultSet.getMetaData();
			int noCols = metaData.getColumnCount();
			
			java.util.Date currentDate = (java.util.Date) new java.util.Date();
			while (resultSet.next()) {
				for (int i = 1; i <= noCols; i++) {
					Timestamp dtDate = (Timestamp) resultSet.getObject(3);
					Long count = (Long) resultSet.getObject(4);
					
					java.util.Date matchDate = new Date(dtDate.getTime());
					
					long duration  = currentDate.getTime() - matchDate.getTime();

					long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
					
					if ((count == 0 && diffInMinutes <= 240) || (count == 1l && diffInMinutes <= 60) || (count == 2 && diffInMinutes <= 30) || (count == 3 && diffInMinutes <= 15)) {
						return true;
					}
				}
			}
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return false;
	}

	public static int checkExist(String value) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			preparedStatement = connection.prepareStatement("Select FixtureId from Fixture where value = ?");
			preparedStatement.setString(1, value);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				return (Integer) rs.getObject(1);
			}
			return 0;
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return 0;
	}
}
