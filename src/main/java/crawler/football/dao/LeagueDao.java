package crawler.football.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import crawler.football.dbmodel.LeagueModel;





public class LeagueDao {

	public void save(LeagueModel league) {
		if (league.getName() == null || "".equals(league.getName()) == true || existed(league) == true)
			return;
		String sql = "INSERT INTO league(name) " + "VALUES (?)";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {
			preStm.setString(1, league.getName());
			preStm.execute();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}

	public void save(List<LeagueModel> leagues) {
		for (LeagueModel league : leagues) {
			save(league);
		}
	}

	public  int getIdByName(String name) {
		String sql = "select id from league where lower(name)=?";
		int id = 0;
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {

			preStm.setString(1, (name == null ? "" : name).toLowerCase());
			ResultSet rs = preStm.executeQuery();

			if (rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return id;
	}

	public boolean existed(LeagueModel league) {
		boolean existed = false;
		String sql = "select count(*) as total from league where lower(name)=?";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {

			preStm.setString(1, (league.getName() == null ? "" : league.getName()).toLowerCase());
			ResultSet rs = preStm.executeQuery();
			int total = 0;
			if (rs.next()) {
				total = rs.getInt(1);
			}
			if (total > 0) {
				existed = true;
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return existed;
	}

}
