package crawler.football.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


public class CommonDao {

	public static void clearDb() {
		try (Connection con = DataSource.getConnection(); Statement stm = con.createStatement();) {
			stm.execute("truncate movement");
			stm.execute("truncate team");
			stm.execute("truncate league");
			stm.execute("truncate event");
			stm.execute("truncate movement");
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public static void clearHistoryDb() {
		try (Connection con = DataSource.getConnection(); Statement stm = con.createStatement();) {
			stm.execute("DELETE FROM movement WHERE TIMESTAMPDIFF(DAY, STR_TO_DATE(crawl_time, '%Y%m%D'), now()) > 3");
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}
