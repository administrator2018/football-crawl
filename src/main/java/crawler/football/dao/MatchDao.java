package crawler.football.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import crawler.football.dbmodel.MatchModel;

public class MatchDao {

	public void save(MatchModel match) {
		if (existed(match) == true)
			return;
		String sql = "INSERT INTO event(home_id, away_id, start_time, url) " + "VALUES (?,?,?,?)";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {
			preStm.setInt(1, match.getHomeId());
			preStm.setInt(2, match.getAwayId());
			preStm.setString(3, match.getStartTime());
			preStm.setString(4, match.getUrl());
			preStm.execute();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}

	public  int getId(MatchModel match) {
		String sql = "select id from event where home_id=? and away_id=? and start_time = ?";
		int id = 0;
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {

			preStm.setInt(1, match.getHomeId());
			preStm.setInt(2, match.getAwayId());
			preStm.setString(3, match.getStartTime());
			ResultSet rs = preStm.executeQuery();

			if (rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return id;
	}
	
	public  List<MatchModel> findAll() {
		List<MatchModel> matchModels = new ArrayList<>();
		String sql = "select * from event";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {


			ResultSet rs = preStm.executeQuery();

			while (rs.next()) {
				MatchModel matchModel= new MatchModel();
				matchModel.setHomeId(rs.getInt("home_id"));
				matchModel.setAwayId(rs.getInt("away_id"));
				matchModel.setStartTime(rs.getString("start_time"));
				matchModel.setUrl(rs.getString("url"));
				matchModels.add(matchModel);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return matchModels;
	}

	public void save(List<MatchModel> matches) {
		for (MatchModel match : matches) {
			save(match);
		}
	}

	public boolean existed(MatchModel match) {
		boolean existed = false;
		String sql = "select count(*) as total from event where home_id=? and away_id=? and start_time = ?";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {

			preStm.setInt(1, match.getHomeId());
			preStm.setInt(2, match.getHomeId());
			preStm.setString(3, match.getStartTime());
			ResultSet rs = preStm.executeQuery();
			int total = 0;
			if (rs.next()) {
				total = rs.getInt(1);
			}
			if (total > 0) {
				existed = true;
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return existed;
	}

}
