package crawler.football.dao;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DataSource {

	private static HikariConfig config = new HikariConfig();
	private static HikariDataSource ds;

	static {
		config.setJdbcUrl("jdbc:mysql://localhost/football?autoReconnect=true&useSSL=false");
		config.setUsername("root");
		config.setPassword("123qwe");
		config.addDataSourceProperty("cachePrepStmts", "true");
		config.addDataSourceProperty("prepStmtCacheSize", "250");
		config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
		ds = new HikariDataSource(config);
	    ds.setMaximumPoolSize(60);
	    ds.setMaxLifetime(60000);
	    ds.setMinimumIdle(13);
	    ds.setIdleTimeout(30000);
	    ds.setLeakDetectionThreshold(60);
	}

	private DataSource() {
	}

	public static Connection getConnection() throws SQLException {
		return ds.getConnection();
	}
}