package crawler.football.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class TeamOLD {

	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DATABASE_URL = "jdbc:mysql://103.252.253.88:3307/football";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "1132426aA@";

	public static void createTeam(String value, String name) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			preparedStatement = connection.prepareStatement("INSERT INTO TeamOLD(Value,Name) VALUES(?,?)");
			preparedStatement.setString(1, value);
			preparedStatement.setString(2, name);
			boolean b = preparedStatement.execute();
			if (b == true)
				System.out.println("1 record inserted...");
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}

	public static void updateSalary(int id, int raise) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			preparedStatement = connection.prepareStatement("UPDATE emp SET salary=salary+? WHERE id=?");
			preparedStatement.setInt(1, raise);
			preparedStatement.setInt(2, id);
			boolean b = preparedStatement.execute();
			if (b == true)
				System.out.println("$" + raise + " raised for emp id=" + id);
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}

	public static void deleteTeam(int id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			preparedStatement = connection.prepareStatement("DELETE FROM emp WHERE id=?");
			preparedStatement.setInt(1, id);
			boolean b = preparedStatement.execute();
			if (b == true)
				System.out.println("1 record deleted...");
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}

	public static void readTeams() {
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM emp");
			ResultSetMetaData metaData = resultSet.getMetaData();
			int noCols = metaData.getColumnCount();
			for (int i = 1; i <= noCols; i++) {
				if (i != 3)
					System.out.printf("%-10s\t", metaData.getColumnName(i).toUpperCase());
			}
			System.out.println();
			while (resultSet.next()) {
				for (int i = 1; i <= noCols; i++) {
					if (i != 3)
						System.out.printf("%-10s\t", resultSet.getObject(i));
				}
				System.out.println();
			}
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}

	public static void readTeam(int id) {
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM emp WHERE id=" + id);
			ResultSetMetaData metaData = resultSet.getMetaData();
			int noCols = metaData.getColumnCount();
			for (int i = 1; i <= noCols; i++) {
				if (i != 3)
					System.out.printf("%-10s\t", metaData.getColumnName(i).toUpperCase());
			}
			System.out.println();
			while (resultSet.next()) {
				for (int i = 1; i <= noCols; i++) {
					if (i != 3)
						System.out.printf("%-10s\t", resultSet.getObject(i));
				}
				System.out.println();
			}
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
	}
	
	public static Integer checkExist(String value) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			Class.forName(JDBC_DRIVER);
			connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
			preparedStatement = connection.prepareStatement("Select TeamId from TeamOLD where value = ?");
			preparedStatement.setString(1, value);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				return (Integer) rs.getObject(1);
			}
			return 0;
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException clsNotFoundEx) {
			clsNotFoundEx.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				connection.close();
			} catch (Exception e) {
			}
		}
		return 0;
	}
	
	
}
