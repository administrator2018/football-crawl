package crawler.football.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import crawler.football.dbmodel.TeamModel;

public class TeamDao {

	public void save(TeamModel team) {
		if (team.getName() == null || "".equals(team.getName()) == true || existed(team) == true)
			return;
		String sql = "INSERT INTO team(name, league_id) " + "VALUES (?,?)";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {
			preStm.setString(1, team.getName());
			preStm.setInt(2, team.getLeagueId());
			preStm.execute();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}

	public void save(List<TeamModel> teams) {
		for (TeamModel team : teams) {
			save(team);
		}
	}

	public int getIdByName(String name) {
		String sql = "select id from team where lower(name)=?";
		int id = 0;
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {

			preStm.setString(1, (name == null ? "" : name).toLowerCase());
			ResultSet rs = preStm.executeQuery();

			if (rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return id;
	}

	public String getNameById(int id) {
		String sql = "select name from team where id=?";
		String name = null;
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {

			preStm.setInt(1, id);
			ResultSet rs = preStm.executeQuery();

			if (rs.next()) {
				name = rs.getString(1);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return name;
	}

	public boolean existed(TeamModel team) {
		boolean existed = false;
		String sql = "select count(*) as total from team where lower(name)=?";
		try (Connection con = DataSource.getConnection(); PreparedStatement preStm = con.prepareStatement(sql);) {

			preStm.setString(1, (team.getName() == null ? "" : team.getName()).toLowerCase());
			ResultSet rs = preStm.executeQuery();
			int total = 0;
			if (rs.next()) {
				total = rs.getInt(1);
			}
			if (total > 0) {
				existed = true;
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return existed;
	}

}
