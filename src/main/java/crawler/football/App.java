package crawler.football;

import java.util.Timer;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import crawler.football.dao.CommonDao;
import crawler.football.scheduler.BetvictorDetailTask;
import crawler.football.scheduler.BetvictorMasterTask;
import crawler.football.scheduler.M88DetailTask;
import crawler.football.scheduler.M88MasterTask;
import crawler.football.scheduler.SignTask;
import crawler.football.service.BetVictorDetailsService;
import crawler.football.service.M88Service;


/**
 * Hello world!
 *
 */
public class App {
	private static final Logger logger = LogManager.getLogger(App.class);
	private static BetVictorDetailsService betVictorService;
	private static M88Service m88Service;
	public static void run() {
		try {
			logger.info("Start crawl ");
			//System.out.println("Ga beo");
			//Xoa di cac du lieu truoc day 3 ngay de giam dung luong db
			//History.deleteHistory();
			//CommonDao.clearDb();
			CommonDao.clearHistoryDb();
			
			Timer bvMasterTimer = new Timer();
			bvMasterTimer.schedule(new BetvictorMasterTask(),2 * 1000, 30*60 * 1000);
			
			Timer bvDetailTimer = new Timer();
			bvDetailTimer.schedule(new BetvictorDetailTask(), 2 * 1000, 2*60 * 1000);				
			
			Timer m88MasterTimer = new Timer();
			m88MasterTimer.schedule(new M88MasterTask(), 2 * 1000, 30*60 * 1000);	
			
			Timer m88DetailTimer = new Timer();
			m88DetailTimer.schedule(new M88DetailTask(), 2 * 1000, 2*60 * 1000);	
			
			Timer signTask = new Timer();
			signTask.schedule(new SignTask(), 2 * 1000, 60 * 1000);			
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args)  {
		run();
	}
}
