package crawler.football.common;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

public class WebDriverController {
	private static int DEFAULT_WAIT_TIMEOUT = 180000; /* milliseconds */
	private static final int DEFAULT_SLEEP_TIME = 250; /* milliseconds */
	private WebDriver webDriver;

	public WebDriverController(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	/* click element on page when element visible */
	public void clickElement(WebElement element) throws WebDriverException {
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(webDriver);
		wait.withTimeout(DEFAULT_WAIT_TIMEOUT, TimeUnit.MILLISECONDS);
		wait.pollingEvery(DEFAULT_SLEEP_TIME, TimeUnit.MILLISECONDS);
		wait.ignoring(NoSuchElementException.class);
		wait.ignoring(StaleElementReferenceException.class);

		WebElement _element = wait.until(ExpectedConditions.elementToBeClickable(element));
		_element.click();
	}

	public void waitLoaderCompleted(By locator) throws WebDriverException {

		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(webDriver);
		wait.withTimeout(DEFAULT_WAIT_TIMEOUT, TimeUnit.MILLISECONDS);
		wait.pollingEvery(DEFAULT_SLEEP_TIME, TimeUnit.MILLISECONDS);
		wait.ignoring(NoSuchElementException.class);
		wait.ignoring(StaleElementReferenceException.class);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		try {
			Thread.sleep(DEFAULT_SLEEP_TIME);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void waitClickable(By locator) throws WebDriverException {

		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(webDriver);
		wait.withTimeout(DEFAULT_WAIT_TIMEOUT, TimeUnit.MILLISECONDS);
		wait.pollingEvery(DEFAULT_SLEEP_TIME, TimeUnit.MILLISECONDS);
		wait.ignoring(NoSuchElementException.class);
		wait.ignoring(StaleElementReferenceException.class);
		wait.until(ExpectedConditions.elementToBeClickable(locator));
		try {
			Thread.sleep(DEFAULT_SLEEP_TIME);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
