package crawler.football.model;

import java.util.List;

import crawler.football.dbmodel.MatchModel;
import crawler.football.dbmodel.MovementModel;

public class Match implements  Cloneable {
	private String startTime;
	private String home;
	private String away;
	private String league;
	private FixOdd fixOdd;
	private transient OverUnder overUnder;
	private transient Handicap hdCap;
	private transient String nextURL;
	List<Handicap> handicap;
	List<OverUnder> ous;
	private Match dummy;
	
	private int status;
	private int homeId;
	private int awayId;
	
	
	
	
	public Match(String startTime, int homeId, int awayId) {
		super();
		this.startTime = startTime;
		this.homeId = homeId;
		this.awayId = awayId;
	}

	public int getHomeId() {
		return homeId;
	}

	public void setHomeId(int homeId) {
		this.homeId = homeId;
	}

	public int getAwayId() {
		return awayId;
	}

	public void setAwayId(int awayId) {
		this.awayId = awayId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	
	public Match() {
	}
	public Match(Match match) {
		this.dummy = match; 
	}
	
	
	public List<OverUnder> getOus() {
		return ous;
	}
	public void setOus(List<OverUnder> ous) {
		this.ous = ous;
	}
	public Match getDummy() {
		return dummy;
	}
	public void setDummy(Match dummy) {
		this.dummy = dummy;
	}
	public List<Handicap> getHandicap() {
		return handicap;
	}
	public void setHandicap(List<Handicap> handicap) {
		this.handicap = handicap;
	}
	public String getNextURL() {
		return nextURL;
	}
	public void setNextURL(String nextURL) {
		this.nextURL = nextURL;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getHome() {
		return home;
	}
	public void setHome(String home) {
		this.home = home;
	}
	public String getAway() {
		return away;
	}
	public void setAway(String away) {
		this.away = away;
	}
	public String getLeague() {
		return league;
	}
	public void setLeague(String league) {
		this.league = league;
	}
	public FixOdd getFixOdd() {
		return fixOdd;
	}
	public void setFixOdd(FixOdd fixOdd) {
		this.fixOdd = fixOdd;
	}
	public OverUnder getOverUnder() {
		return overUnder;
	}
	public void setOverUnder(OverUnder overUnder) {
		this.overUnder = overUnder;
	}
	public Handicap getHdCap() {
		return hdCap;
	}
	public void setHdCap(Handicap hdCap) {
		this.hdCap = hdCap;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + awayId;
		result = prime * result + homeId;
		result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Match other = (Match) obj;
		if (awayId != other.awayId)
			return false;
		if (homeId != other.homeId)
			return false;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		return true;
	}

	
	
	
	
	
	
	
}
