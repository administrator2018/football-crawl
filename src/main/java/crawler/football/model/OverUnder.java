package crawler.football.model;

public class OverUnder {
	private String totalGoal;
	private String over;
	private String under;

	public String getTotalGoal() {
		return totalGoal;
	}
	public void setTotalGoal(String totalGoal) {
		this.totalGoal = totalGoal;
	}
	public String getOver() {
		return over;
	}
	public void setOver(String over) {
		this.over = over;
	}
	public String getUnder() {
		return under;
	}
	public void setUnder(String under) {
		this.under = under;
	}

}
