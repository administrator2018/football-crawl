package crawler.football.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtil {
	public static String getCurrHour() {
		Calendar now = Calendar.getInstance();
		int hour = now.get(Calendar.HOUR_OF_DAY);
		return new Integer(hour).toString();
	}

	public static String getCurrYear() {
		String result = null;
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		result = new Integer(year).toString();
		return result;
	}
	
	public static String getCurrTime() {
		return new SimpleDateFormat("HH:mm").format(new Date());
	}
	
	
	public static String getCurrDateTime() {
		return new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
	}	

	public static void main(String[] args) {
		String currTime = DateTimeUtil.getCurrDateTime();
		String matchTime = "201808141100";
		int minutes = getTimeDiffe(currTime, matchTime, "yyyyMMddHHmm",
				"mm");
		System.out.println(minutes);
	}

	public static Date getStartTime(String startTime) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
		Date dateValue = null;
		try {
			dateValue = sdf.parse(startTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateValue;
	}
	
	public static int getTimeDiffe(String sTime, String eTime,
			String inTimeType, String rsType)  {
		try {
			DateFormat format = new SimpleDateFormat(inTimeType);
			Date d1 = format.parse(sTime);
			Date d2 = format.parse(eTime);
			long between = (d2.getTime() - d1.getTime()) / 1000;
			if (rsType.toLowerCase().equals("dd")) {
				return (int) (between / (24 * 3600));
			} else if (rsType.toUpperCase().equals("HH")) {
				return (int) (between / 3600);
			} else if (rsType.equals("mm")) {
				return (int) (between / 60);
			} else if (rsType.toLowerCase().equals("ss")) {
				return (int) between;
			}
			// 默认返回天数
			return (int) (between / (24 * 3600));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
			
		}
	}	

}
