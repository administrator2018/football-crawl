package crawler.football.utils;

import java.net.HttpURLConnection;
import java.net.URL;

public class CommonMethod {
	public static int getStatus(int minutes) {
		int status = 0;
		if (minutes < 0) {
			status = 0;
		} else if (minutes < 30) {
			status = 4;
		} else if (minutes < 60) {
			status = 3;
		} else if (minutes < 120) {
			status = 2;
		} else if (minutes < 240) {
			status = 1;
		} else if (minutes > 240) {
			status = 0;
		}
		return status;
	}

	
	public static boolean linkExists(String URLName){
	    try {
	      HttpURLConnection.setFollowRedirects(false);
	      // note : you may also need
	      //        HttpURLConnection.setInstanceFollowRedirects(false)
	      HttpURLConnection con =
	         (HttpURLConnection) new URL(URLName).openConnection();
	      con.setRequestMethod("HEAD");
	      System.setProperty("http.agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
	      return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
	    }
	    catch (Exception e) {
	       e.printStackTrace();
	       return false;
	    }
	  }  
	
	public static void main(String[] args) {
		System.out.println(linkExists("https://www.betvictor.com/en-gb/sports/football/coupons/100/15756810/973973600/0/0/PE/0/0/0/0/0"));
	}
}
